/**
 * 分页查询学生列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apStudent/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询学生详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apStudent/query/' + id,
        method: 'get'
    })
}

/**
 * 新增学生
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apStudent/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改学生
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apStudent/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除学生
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apStudent/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出学生
 *
 * @param query
 * @returns {*}
 */
function exportData(data) {
    return requests({
        url: '/admin/apStudent/export',
        method: 'post',
        data: data
    })
}
