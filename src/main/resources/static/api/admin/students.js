/**
 * 分页查询学生成绩列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/students/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询学生成绩详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/students/query/' + id,
        method: 'get'
    })
}

/**
 * 新增学生成绩
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/students/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改学生成绩
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/students/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除学生成绩
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/students/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出学生成绩
 *
 * @param query
 * @returns {*}
 */
function exportData(data) {
    return requests({
        url: '/admin/students/export',
        method: 'post',
        data: data
    })
}
