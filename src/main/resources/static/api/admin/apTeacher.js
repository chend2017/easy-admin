/**
 * 分页查询教师列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apTeacher/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询教师详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apTeacher/query/' + id,
        method: 'get'
    })
}

/**
 * 新增教师
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apTeacher/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改教师
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apTeacher/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除教师
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apTeacher/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出教师
 *
 * @param query
 * @returns {*}
 */
function exportData(data) {
    return requests({
        url: '/admin/apTeacher/export',
        method: 'post',
        data: data
    })
}
