package com.mars.common.response.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户详情VO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class SysUserDetailResponse {

    @ApiModelProperty(value = "用户ID")
    private Long id;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "姓名")
    private String realName;

    @ApiModelProperty(value = "用户类型(0内置用户 1注册用户)")
    private Integer type;

    @ApiModelProperty(value = "性别（1男  2女）")
    private Integer sex;

    @ApiModelProperty(value = "出生日期（yyyy-MM-dd）")
    private LocalDate birthDate;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "角色")
    private List<Long> roleId;

    @ApiModelProperty(value = "用户岗位名称")
    private Long postId;


}
