package com.mars.common.response.router;

import lombok.Data;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2023-11-07 17:19:36
 */
@Data
public class Router {

    /**
     * 请求地址
     */
    private String url;

    /**
     * 跳转地址
     */
    private String path;

}
