package com.mars.common.request.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * 个人资料修改DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class DataUpdateRequest {

    /**
     * ID
     */
    @NotNull
    @ApiModelProperty(value = "ID")
    private Long id;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名称")
    private String userName;

    /**
     * 性别
     */
    @NotNull
    @ApiModelProperty(value = "性别（1男  2女）")
    private Integer sex;

    /**
     * 出生日期
     */
    @NotNull
    @ApiModelProperty(value = "出生日期（yyyy-MM-dd）")
    private LocalDate birthDate;

    /**
     * 手机号码
     */
    @NotEmpty
    @ApiModelProperty(value = "手机号码")
    private String phone;

    /**
     * 地址
     */
    @ApiModelProperty(value = "头像")
    private String avatar;

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    private String address;

    /**
     * 真实名称
     */
    @ApiModelProperty(value = "真实名称")
    private String realName;


}
