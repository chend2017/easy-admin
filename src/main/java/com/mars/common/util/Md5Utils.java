package com.mars.common.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2023-11-15 09:40:34
 */
public class Md5Utils {

    /**
     * md5
     *
     * @param input input
     * @return String
     */
    public static String getMd5(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(input.getBytes());
        byte[] digest = md.digest();
        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(String.format("%02x", b & 0xff));
        }
        return sb.toString();
    }
}
