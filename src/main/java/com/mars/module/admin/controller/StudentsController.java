package com.mars.module.admin.controller;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import com.mars.common.util.ExcelUtils;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.RateLimiter;
import com.mars.module.admin.entity.Students;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import com.mars.common.response.PageInfo;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.IStudentsService;
import com.mars.module.admin.request.StudentsRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * 学生成绩控制层
 *
 * @author mars
 * @date 2024-03-12
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "学生成绩接口管理",tags = "学生成绩接口管理")
@RequestMapping("/admin/students" )
public class StudentsController {

    private final IStudentsService iStudentsService;

    /**
     * 分页查询学生成绩列表
     */
    @ApiOperation(value = "分页查询学生成绩列表")
    @PostMapping("/pageList")
    public R<PageInfo<Students>> pageList(@RequestBody StudentsRequest studentsRequest) {
        return R.success(iStudentsService.pageList(studentsRequest));
    }

    /**
     * 获取学生成绩详细信息
     */
    @ApiOperation(value = "获取学生成绩详细信息")
    @GetMapping(value = "/query/{id}")
    public R<Students> detail(@PathVariable("id") Integer id) {
        return R.success(iStudentsService.getById(id));
    }

    /**
     * 新增学生成绩
     */
    @Log(title = "新增学生成绩", businessType = BusinessType.INSERT)
    @RateLimiter
    @ApiOperation(value = "新增学生成绩")
    @PostMapping("/add")
    public R<Void> add(@RequestBody StudentsRequest studentsRequest) {
        iStudentsService.add(studentsRequest);
        return R.success();
    }

    /**
     * 修改学生成绩
     */
    @Log(title = "修改学生成绩", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改学生成绩")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody StudentsRequest studentsRequest) {
        iStudentsService.update(studentsRequest);
        return R.success();
    }

    /**
     * 删除学生成绩
     */
    @Log(title = "删除学生成绩", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除学生成绩")
    @PostMapping("/delete/{ids}")
    public R<Void> remove(@PathVariable Integer[] ids) {
        iStudentsService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }

    /**
    * 导出学生成绩
    *
    * @param response response
    * @throws IOException IOException
    */
    @PostMapping(value = "/export")
    public void exportExcel(HttpServletResponse response,@RequestBody StudentsRequest studentsRequest) throws IOException {
        List<Students> list = iStudentsService.list(studentsRequest);
        ExcelUtils.exportExcel(list, Students.class, "学生成绩信息", response);
    }
}
