package com.mars.module.admin.controller;


import java.util.Arrays;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.RateLimiter;
import com.mars.module.admin.entity.SysMessage;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import com.mars.common.response.PageInfo;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.ISysMessageService;
import com.mars.module.admin.request.SysMessageRequest;

/**
 * 消息控制层
 *
 * @author mars
 * @date 2023-12-06
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "消息接口管理", tags = "消息接口管理")
@RequestMapping("/admin/sysMessage")
public class SysMessageController {

    private final ISysMessageService iSysMessageService;

    /**
     * 分页查询消息列表
     */
    @ApiOperation(value = "分页查询消息列表")
    @PostMapping("/pageList")
    public R<PageInfo<SysMessage>> pageList(@RequestBody SysMessageRequest sysMessage) {
        return R.success(iSysMessageService.pageList(sysMessage));
    }


    /**
     * 一键已读
     */
    @ApiOperation(value = "一键已读")
    @GetMapping(value = "/readAllMsg")
    public R<Void> readAllMsg() {
        iSysMessageService.readAllMsg();
        return R.success();
    }

    /**
     * 获取消息详细信息
     */
    @ApiOperation(value = "获取消息详细信息")
    @GetMapping(value = "/query/{id}")
    public R<SysMessage> detail(@PathVariable("id") Long id) {
        return R.success(iSysMessageService.getById(id));
    }

    /**
     * 新增消息
     */
    @Log(title = "新增消息", businessType = BusinessType.INSERT)
    @RateLimiter
    @ApiOperation(value = "新增消息")
    @PostMapping("/add")
    public R<Void> add(@RequestBody SysMessageRequest sysMessage) {
        iSysMessageService.add(sysMessage);
        return R.success();
    }

    /**
     * 修改消息
     */
    @Log(title = "修改消息", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改消息")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody SysMessageRequest sysMessage) {
        iSysMessageService.update(sysMessage);
        return R.success();
    }

    /**
     * 删除消息
     */
    @Log(title = "删除消息", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除消息")
    @PostMapping("/delete/{ids}")
    public R<Void> remove(@PathVariable Long[] ids) {
        iSysMessageService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }
}
